package logwriter

import (
	"compress/gzip"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
)

var logger = log.New(os.Stderr, "", log.Llongfile)

func compressFile(path string) error {
	if src, err := os.Open(path); err != nil {
		return err
	} else {
		defer src.Close()
		if dst, err := os.OpenFile(path+compressSuffix, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644); err != nil {
			return err
		} else {
			defer dst.Close()
			gz := gzip.NewWriter(dst)
			defer gz.Close()
			if _, err = io.Copy(gz, src); err != nil {
				return err
			}
			if err = os.Remove(path); err != nil {
				return err
			}
		}
	}
	return nil
}

func isRenameFormatByNumber(format string) bool {
	if format == RenameFormatNumber || format == RenameFormatNumberRetain {
		return true
	}
	return false
}

func isRenameFormatByNumberRetain(format string) bool {
	if format == RenameFormatNumberRetain {
		return true
	}
	return false
}

func numericName(name string) string {
	var numeric uint64
	result := make([]byte, 0)
	stack := false
	for i := 0; i < len(name); i++ {
		if name[i] <= '9' && '0' <= name[i] {
			u8, _ := strconv.ParseUint(string(name[i]), 10, 8)
			if (numeric*10)+u8 < numeric {
				numeric = 0xffffffffffffffff
			} else {
				numeric = (numeric * 10) + u8
			}
			stack = true
		} else if stack {
			result = append(result,
				byte(0xff&(numeric>>56)),
				byte(0xff&(numeric>>48)),
				byte(0xff&(numeric>>40)),
				byte(0xff&(numeric>>32)),
				byte(0xff&(numeric>>24)),
				byte(0xff&(numeric>>16)),
				byte(0xff&(numeric>>8)),
				byte(0xff&numeric),
				name[i],
			)
			stack = false
			numeric = 0
		} else {
			result = append(result, name[i])
		}
	}
	if stack {
		result = append(result,
			byte(0xff&(numeric>>56)),
			byte(0xff&(numeric>>48)),
			byte(0xff&(numeric>>40)),
			byte(0xff&(numeric>>32)),
			byte(0xff&(numeric>>24)),
			byte(0xff&(numeric>>16)),
			byte(0xff&(numeric>>8)),
			byte(0xff&numeric),
		)
	}
	return string(result)
}

func readDirBackupFile(info filePathInfo, config *logConfig) ([]os.DirEntry, error) {
	var filterFunc, backupFilterFunc func(string) bool
	var sortFunc, backupSortFunc func(int, int) bool
	var resultDirs, backupDirs []os.DirEntry
	var baseLen int
	isNumber := isRenameFormatByNumber(config.renameFormat)
	isNumberRetain := isRenameFormatByNumberRetain(config.renameFormat)

	if !config.renameBeforeExt {
		filterFunc = func(name string) bool {
			return strings.HasPrefix(name, info.base+config.renameJoinString) && name != info.base
		}
		backupFilterFunc = func(name string) bool {
			return strings.HasPrefix(name, info.base+config.renameJoinString+string(backupTempChar)) && name != info.base
		}
		baseLen = len(info.base) + len(config.renameJoinString)
	} else {
		filterFunc = func(name string) bool {
			return strings.HasPrefix(name, info.stem+config.renameJoinString) && !strings.HasPrefix(name, info.base) && (strings.HasSuffix(name, info.ext) || strings.HasSuffix(name, info.ext+compressSuffix))
		}
		backupFilterFunc = func(name string) bool {
			return strings.HasPrefix(name, info.stem+config.renameJoinString+string(backupTempChar)) && !strings.HasPrefix(name, info.base) && (strings.HasSuffix(name, info.ext) || strings.HasSuffix(name, info.ext+compressSuffix))
		}
		baseLen = len(info.stem) + len(config.renameJoinString)
	}

	if isNumberRetain {
		sortFunc = func(i, j int) bool {
			return numericName(resultDirs[i].Name()[baseLen:]) > numericName(resultDirs[j].Name()[baseLen:])
		}
		backupSortFunc = func(i, j int) bool {
			return numericName(backupDirs[i].Name()[baseLen:]) < numericName(backupDirs[j].Name()[baseLen:])
		}
	} else if isNumber {
		sortFunc = func(i, j int) bool {
			return numericName(resultDirs[i].Name()[baseLen:]) > numericName(resultDirs[j].Name()[baseLen:])
		}
	} else {
		sortFunc = func(i, j int) bool {
			return resultDirs[i].Name() > resultDirs[j].Name()
		}
	}

	f, err := os.Open(info.dir)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	dirs, err := f.ReadDir(-1)
	if err == nil {
		for _, f := range dirs {
			name := f.Name()
			if !f.IsDir() {
				if isNumberRetain && backupFilterFunc(name) {
					backupDirs = append(backupDirs, f)
				} else if filterFunc(name) {
					resultDirs = append(resultDirs, f)
				}
			}
		}
		sort.Slice(resultDirs, sortFunc)
		sort.Slice(backupDirs, backupSortFunc)
	}
	return append(resultDirs, backupDirs...), err
}

func getLastNumber(info filePathInfo, config *logConfig) int {
	var lastNum, num int

	files, _ := readDirBackupFile(info, config)
	for _, f := range files {
		name := f.Name()
		if !config.renameBeforeExt {
			baseLen := len(info.base) + len(config.renameJoinString)
			if filepath.Ext(name) == compressSuffix {
				num, _ = strconv.Atoi(name[baseLen : len(name)-len(compressSuffix)])
			} else {
				num, _ = strconv.Atoi(name[baseLen:])
			}
		} else {
			baseLen := len(info.stem) + len(config.renameJoinString)
			if filepath.Ext(name) == compressSuffix {
				num, _ = strconv.Atoi(name[baseLen : len(name)-len(info.ext)-len(compressSuffix)])
			} else {
				num, _ = strconv.Atoi(name[baseLen : len(name)-len(info.ext)])
			}
		}
		if num > lastNum {
			lastNum = num
		}
	}
	return lastNum
}

func getBackupName(dir string, base string, join string, suffix string, ext string) string {
	return filepath.Join(dir, fmt.Sprintf("%s%s%s%s", base, join, suffix, ext))
}
