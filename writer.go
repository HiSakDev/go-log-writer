package logwriter

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

type filePathInfo struct {
	path string
	dir  string
	base string
	stem string
	ext  string
}

type FileWriter struct {
	file        *os.File
	config      *logConfig
	mutex       sync.Mutex
	reopenMutex sync.Mutex
	wait        sync.WaitGroup
	info        filePathInfo
	size        int64
	event       chan bool
	nextNumber  int
	buf         *bufio.Writer
}

func NewFileWriter(name string, format string, opts ...LogConfigOption) (w *FileWriter, err error) {
	var info filePathInfo
	if info.path, err = filepath.Abs(name); err == nil {
		info.dir = filepath.Dir(info.path)
		info.base = filepath.Base(info.path)
		info.ext = filepath.Ext(info.base)
		info.stem = info.base[:len(info.base)-len(info.ext)]
		w = &FileWriter{
			info:   info,
			config: newConfig(format, opts...),
			event:  make(chan bool, 1),
		}
		if !w.config.disableRolling {
			if isRenameFormatByNumber(w.config.renameFormat) {
				w.nextNumber = getLastNumber(info, w.config) + 1
			}
			w.wait.Add(1)
			go func() {
				defer w.wait.Done()
				for range w.event {
					if errRolling := w.doRolling(); errRolling != nil {
						logger.Println(errRolling)
					}
				}
			}()
		}
		if !w.config.delayFileOpen {
			if err = w.openInitialFile(0); err != nil {
				w = nil
			}
		}
	}
	return
}

func (w *FileWriter) openInitialFile(nextWriteLen int) (err error) {
	var info os.FileInfo

	if w.event == nil {
		return errors.New(fmt.Sprintf("openInitialFile %s: file already closed", w.info.path))
	}

	if info, err = os.Stat(w.info.path); os.IsNotExist(err) {
		if err = os.MkdirAll(filepath.Dir(w.info.path), 0755); err != nil {
			return err
		}
	} else if err != nil {
		return err
	} else {
		if !w.config.disableRolling && info.Size()+int64(nextWriteLen) > w.config.maxSize {
			return w.reopenFile()
		}
	}

	if w.file, err = os.OpenFile(w.info.path, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644); err != nil {
		return err
	} else {
		if info, err = w.file.Stat(); err != nil {
			return err
		} else {
			w.size = info.Size()
			if w.config.bufferSize > 0 {
				w.buf = bufio.NewWriterSize(w.file, w.config.bufferSize)
			}
		}
	}
	return nil
}

func (w *FileWriter) reopenFile() (err error) {
	w.reopenMutex.Lock()
	defer w.reopenMutex.Unlock()

	backupName := w.backupName()
	if err = w.closeFile(); err != nil {
		return err
	}
	if err = os.Rename(w.info.path, backupName); err != nil {
		return err
	} else {
		if isRenameFormatByNumber(w.config.renameFormat) {
			w.nextNumber += 1
		}
		if w.file, err = os.OpenFile(w.info.path, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644); err != nil {
			return err
		} else {
			w.size = 0
			if w.config.bufferSize > 0 {
				if w.buf != nil {
					w.buf.Reset(w.file)
				} else {
					w.buf = bufio.NewWriterSize(w.file, w.config.bufferSize)
				}
			}
		}
	}

	select {
	case w.event <- true:
	default:
	}
	return nil
}

func (w *FileWriter) closeFile() error {
	var errBuf, errFile error
	if w.buf != nil {
		errBuf = w.buf.Flush()
	}
	if w.file != nil {
		errFile = w.file.Close()
		w.file = nil
	}
	if errBuf != nil {
		return errBuf
	} else if errFile != nil {
		return errFile
	}
	return nil
}

func (w *FileWriter) doRolling() error {
	if files, err := readDirBackupFile(w.info, w.config); err != nil {
		return err
	} else {
		count := 0
		if isRenameFormatByNumberRetain(w.config.renameFormat) {
			count = len(files)
			for _, f := range files {
				name := f.Name()
				if w.config.maxBackups < count {
					if err = os.Remove(filepath.Join(w.info.dir, name)); err != nil {
						return err
					}
				} else {
					var newName string
					oldName := filepath.Join(w.info.dir, name)
					if !w.config.renameBeforeExt {
						newName = getBackupName(w.info.dir, w.info.base, w.config.renameJoinString, strconv.Itoa(count), "")
					} else {
						newName = getBackupName(w.info.dir, w.info.stem, w.config.renameJoinString, strconv.Itoa(count), w.info.ext)
					}
					if filepath.Ext(name) == compressSuffix {
						newName += compressSuffix
					}
					if oldName != newName {
						if err = os.Rename(oldName, newName); err != nil {
							return err
						}
					}
					if w.config.compress && !strings.HasSuffix(newName, compressSuffix) {
						if err = compressFile(newName); err != nil {
							return err
						}
					}
				}
				count -= 1
			}

			if w.config.maxBackups+1 < w.nextNumber {
				w.reopenMutex.Lock()
				defer w.reopenMutex.Unlock()
				w.nextNumber = getLastNumber(w.info, w.config) + 1
			}
		} else {
			for _, f := range files {
				name := f.Name()
				count += 1
				if w.config.maxBackups < count {
					if err = os.Remove(filepath.Join(w.info.dir, name)); err != nil {
						return err
					}
				} else if w.config.compress && !strings.HasSuffix(name, compressSuffix) {
					if err = compressFile(filepath.Join(w.info.dir, name)); err != nil {
						return err
					}
				}
			}
		}
	}

	return nil
}

func (w *FileWriter) backupName() string {
	now := time.Now()
	var backupSuffix string
	switch w.config.renameFormat {
	case "":
		fallthrough
	case RenameFormatUnix:
		backupSuffix = strconv.FormatInt(now.Unix(), 10)
	case RenameFormatUnixNano:
		backupSuffix = strconv.FormatInt(now.UnixNano(), 10)
	case RenameFormatRFC3339:
		backupSuffix = now.Format(time.RFC3339)
	case RenameFormatRFC3339Nano:
		backupSuffix = now.Format(time.RFC3339Nano)
	case RenameFormatNumberRetain:
		backupSuffix = string(backupTempChar)
		fallthrough
	case RenameFormatNumber:
		backupSuffix += strconv.Itoa(w.nextNumber)
	default:
		backupSuffix = now.Format(w.config.renameFormat)
	}

	if !w.config.renameBeforeExt {
		return getBackupName(w.info.dir, w.info.base, w.config.renameJoinString, backupSuffix, "")
	}
	return getBackupName(w.info.dir, w.info.stem, w.config.renameJoinString, backupSuffix, w.info.ext)
}

func (w *FileWriter) Rotate(size int64) error {
	w.mutex.Lock()
	defer w.mutex.Unlock()

	if !w.config.disableRolling && w.size >= size {
		return w.reopenFile()
	}

	return nil
}

func (w *FileWriter) Sync() error {
	w.mutex.Lock()
	defer w.mutex.Unlock()

	var errBuf, errFile error
	if w.buf != nil {
		errBuf = w.buf.Flush()
	}
	if w.file != nil {
		errFile = w.file.Sync()
	}
	if errBuf != nil {
		return errBuf
	} else if errFile != nil {
		return errFile
	}
	return nil
}

func (w *FileWriter) Write(b []byte) (n int, err error) {
	w.mutex.Lock()
	defer w.mutex.Unlock()

	writeLen := len(b)

	if w.file == nil {
		err = w.openInitialFile(writeLen)
	} else {
		if !w.config.disableRolling && w.size+int64(writeLen) > w.config.maxSize {
			err = w.reopenFile()
		}
	}

	if err != nil {
		return 0, err
	} else {
		if w.buf != nil {
			n, err = w.buf.Write(b)
		} else {
			n, err = w.file.Write(b)
		}
		w.size += int64(n)
	}

	return
}

func (w *FileWriter) Close() error {
	w.mutex.Lock()
	defer w.mutex.Unlock()

	if w.event != nil {
		close(w.event)
		w.wait.Wait()
		w.event = nil
	}
	return w.closeFile()
}
