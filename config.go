package logwriter

const (
	compressSuffix = ".gz"
	backupTempChar = '0'

	RenameFormatUnix         = "Unix"
	RenameFormatUnixNano     = "UnixNano"
	RenameFormatRFC3339      = "RFC3339"
	RenameFormatRFC3339Nano  = "RFC3339Nano"
	RenameFormatNumber       = "Number"
	RenameFormatNumberRetain = "NumberRetain"
)

type logConfig struct {
	disableRolling   bool
	maxBackups       int
	maxSize          int64
	compress         bool
	delayFileOpen    bool
	renameJoinString string
	renameBeforeExt  bool
	renameFormat     string
	bufferSize       int
}

type LogConfigOption func(config *logConfig)

func newConfig(format string, opts ...LogConfigOption) *logConfig {
	defaultJoin := "-"
	if isRenameFormatByNumber(format) {
		defaultJoin = "."
	}
	config := &logConfig{
		renameFormat:     format,
		renameJoinString: defaultJoin,
	}
	for _, opt := range opts {
		opt(config)
	}
	if config.maxBackups < 1 {
		// rolling is disabled if maxBackups less than 1
		config.disableRolling = true
	}
	if config.maxSize < 1024 {
		// max size is default value (1 MB) if less than 1 kb
		config.maxSize = 1024 * 1024
	}
	if config.bufferSize < 1024 {
		// buffer size is default disable (0) if less than 1 kb
		config.bufferSize = 0
	}
	return config
}

func WithMaxBackups(num int) LogConfigOption {
	return func(ops *logConfig) {
		ops.maxBackups = num
	}
}

func WithMaxSize(size int64) LogConfigOption {
	return func(ops *logConfig) {
		ops.maxSize = size
	}
}

func WithMaxSizeMB(size int64) LogConfigOption {
	return WithMaxSize(size * 1024 * 1024)
}

func WithCompress() LogConfigOption {
	return func(ops *logConfig) {
		ops.compress = true
	}
}

func WithDelayFileOpen() LogConfigOption {
	return func(ops *logConfig) {
		ops.delayFileOpen = true
	}
}

func WithRenameJoinString(join string) LogConfigOption {
	return func(ops *logConfig) {
		ops.renameJoinString = join
	}
}

func WithRenameBeforeExt() LogConfigOption {
	return func(ops *logConfig) {
		ops.renameBeforeExt = true
	}
}

func WithBufferSize(size int) LogConfigOption {
	return func(ops *logConfig) {
		ops.bufferSize = size
	}
}

func WithBufferSizeKB(size int) LogConfigOption {
	return WithBufferSize(size * 1024)
}
