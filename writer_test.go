package logwriter

import (
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"testing"
)

func makeBenchmarkSeed() ([]byte, string) {
	data := make([]byte, 1024)
	rand.Read(data)
	temp, err := ioutil.TempDir("", "logwriter")
	if err != nil {
		panic(err)
	}
	return data, temp
}

func BenchmarkFileWriter(b *testing.B) {
	data, temp := makeBenchmarkSeed()
	w, err := NewFileWriter(
		filepath.Join(temp, "bench.dat"),
		RenameFormatNumberRetain,
		WithMaxBackups(10),
		WithMaxSizeMB(1),
	)
	if err != nil {
		panic(err)
	}
	defer func() {
		w.Close()
		os.RemoveAll(temp)
	}()

	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		w.Write(data)
	}
}

func BenchmarkParallelFileWriter(b *testing.B) {
	data, temp := makeBenchmarkSeed()
	w, err := NewFileWriter(
		filepath.Join(temp, "bench.dat"),
		RenameFormatNumberRetain,
		WithMaxBackups(10),
		WithMaxSizeMB(1),
	)
	if err != nil {
		panic(err)
	}
	defer func() {
		w.Close()
		os.RemoveAll(temp)
	}()

	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			w.Write(data)
		}
	})
}

func BenchmarkBufferWriter(b *testing.B) {
	data, temp := makeBenchmarkSeed()
	w, err := NewFileWriter(
		filepath.Join(temp, "bench.dat"),
		RenameFormatNumberRetain,
		WithMaxBackups(10),
		WithMaxSizeMB(1),
		WithBufferSizeKB(4),
	)
	if err != nil {
		panic(err)
	}
	defer func() {
		w.Close()
		os.RemoveAll(temp)
	}()

	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		w.Write(data)
	}
}

func BenchmarkParallelBufferWriter(b *testing.B) {
	data, temp := makeBenchmarkSeed()
	w, err := NewFileWriter(
		filepath.Join(temp, "bench.dat"),
		RenameFormatNumberRetain,
		WithMaxBackups(10),
		WithMaxSizeMB(1),
		WithBufferSizeKB(4),
	)
	if err != nil {
		panic(err)
	}
	defer func() {
		w.Close()
		os.RemoveAll(temp)
	}()

	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			w.Write(data)
		}
	})
}
